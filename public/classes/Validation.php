<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 26.10.2016
 * Time: 10:12
 *
 * This class validates user input by given rules
 */
class Validation
{
    private $_passed = false,
        $_errors = array(),
        $_db = null;

    public function __construct() {
        $this->_db = DB::getInstance();
    }

    /**
     * starts validation process for a source and rules
     * @param array $source userinput array e.g. $_POST where key is field and value is input data
     * @param array $items rules where the first dimension of the array is build with the keys of the userdata like "password"
     * and the second dimension shows the rules and their values like "min" => 6  -  array("password" => array("min" => 6) )
     * @return $this self instance
     */
    public function check($source, $items = array()) {
        // only every item should be checked which also has a rule, so loop through rule items instead of source
        foreach($items as $item => $rules) {
            // now loop through their rules to check every value of the source against it
            foreach($rules as $rule => $rule_value) {
                // extract values for rule and source
                $value = trim($source[$item]);
                $item = escape($item);
                $rule = escape($rule);
                $rule_value = escape($rule_value);

                if($rule === "required" && empty($value)){
                    $this->addError("$item is required");
                } else if(!empty($value)) {
                    switch($rule) {
                        //TODO: Only allow alphanumeric not numbers only
                        case "min":
                            if(strlen($value) < $rule_value){
                                $this->addError($this->beautify($item) ." muss mindestens $rule_value Zeichen enthalten");
                            }
                            break;
                        case "max":
                            if(strlen($value) > $rule_value) {
                                $this->addError($this->beautify($item) ." darf höchstens $rule_value Zeichen enthalten");
                            }
                            break;
                        case "matches":
                            // checking other item which is not available in this loop
                            if($value != $source[$rule_value]) {
                                $this->addError($this->beautify($rule_value) . " muss " . $this->beautify($item) ." gleichen");
                            }
                            break;
                        case "unique":
                            $check = $this->_db->get($rule_value, array($item,"=",$value));
                            if($check->count()) {
                                $this->addError($this->beautify($item) ." existiert bereits");
                            }
                            break;
                        case "type":
                            if($rule_value === "email") {
                                if(filter_var($value, FILTER_VALIDATE_EMAIL) === false){
                                    $this->addError("E-Mail Adresse im falschen Format");
                                }
                            }
                            break;
                        case "minval":
                            if($value < $rule_value) {
                                $this->addError("Keine Nutzerrolle ausgewählt");
                            }
                    }
                }
            }
        }

        if(empty($this->_errors)) {
            $this->_passed = true;
        }

        return $this;
    }

    /**
     * rewrites keys for useful error messages
     * @param $string
     * @return string
     */
    public function beautify($string) {
        switch($string) {
            case "username":
                return "Benutzername";
            case "password":
                return "Passwort";
            case "password_again":
                return "Passwort wiederholen";
            case "email":
                return "E-Mail";
            default:
                return $string;
        }
    }

    private function addError($error) {
        $this->_errors[] = $error;
    }

    public function errors() {
        return $this->_errors;
    }

    public function passed() {
        return $this->_passed;
    }
}