<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 26.10.2016
 * Time: 10:12
 *
 * Input class helper to get userinput a lot easier and independent from get or post method
 */
class Input
{
    /**
     * checks wether input exists
     * @param string $type get or post? Default is post...
     * @return bool true if found false if not
     */
    public static function exists($type = "post")
    {
        switch ($type) {
            case "post":
                return (!empty($_POST)) ? true : false;
                break;
            case "get":
                return (!empty($_GET)) ? true : false;
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * gets a value from the user input
     * @param string $item the key of which the value should be returned
     * @return string|bool the value or false if it wasn't found
     */
    public static function get($item)
    {
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } else if (isset($_GET[$item])) {
            return $_GET[$item];
        }
        return false;
    }
}