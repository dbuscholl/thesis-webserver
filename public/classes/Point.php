<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 16.02.2017
 * Time: 11:54
 *
 * Simple point entity class holding coordinates of a point on a map and the id of the "person" for which it stands for
 */
class Point
{
    private $_id,$latitude,$longitude;

    public static function make($i,$la,$lo){
        $p = new Point();
        $p->setId($i);
        $p->setLatitude($la);
        $p->setLongitude($lo);
        return $p;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }


}