<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 26.10.2016
 * Time: 12:46
 *
 * Simple class providing basic hashing functions
 */
class Hash
{
    /**
     * creates md5 hash of a given string with optional salt
     * @param string $string string to be hashed
     * @param string $salt hopefully a complex salt which should be appended
     * @return string the hash as string
     */
    public static function make($string, $salt = ''){
        return hash("md5",$string . $salt);
    }

    /**
     * creates a crazy salt using mcrypt
     * @param int $length size of the salt
     * @return string the salt as string
     */
    public static function salt($length) {
        return mcrypt_create_iv($length);
    }

    /**
     * creates an unique id, use secureToken() instead
     * @return string the unique id
     */
    public static function unique() {
        return self::make(uniqid());
    }

    /**
     * creates a unique id by coding 32 pseudo bytes into a hexadecimal string
     * @return string the unqiue hex string
     */
    public static function secureToken() {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }
}