<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 21.11.2016
 * Time: 14:32
 *
 * this class manages any event operations coming from the index.php / HTTP Request
 */
class Eventcontrol
{
    /**
     * creates new events from post data
     * @return string json string with success-information
     */
    public static function newEvent()
    {
        if (Input::exists()) {
            // validating input
            $user = Usercontrol::validate(false);
            if ($user) {
                if ($user->hasRole(1)) {
                    $val = new Validation();
                    $val->check($_POST, array(
                        "name" => array(
                            "required" => true,
                            "max" => 255,
                            "min" => 3
                        ),
                        "adresse" => array(
                            "required" => true,
                            "min" => 3,
                            "max" => 300
                        )
                    ));
                    if (!$val->passed()) {
                        $errors = $val->errors();
                        $error = "";
                        foreach ($errors as $e) {
                            $error .= $e . "\n";
                        }
                        return json_encode(array("success" => false, "error" => $error));
                    }

                    $event = new Event();
                    try {
                        // geocode address
                        $urlparam = str_replace(' ', '+', Input::get("adresse"));
                        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $urlparam;
                        $json = file_get_contents($url);
                        $json = json_decode($json, true);
                        $lat = $json["results"][0]["geometry"]["location"]["lat"];
                        $lng = $json["results"][0]["geometry"]["location"]["lng"];
                        if (!$lat || !$lng) {
                            return json_encode(array("success" => false, "error" => "Probleme mit dem Geocoding Service"));
                        }

                        // put event into DB
                        $event->create(array(
                            "personenId" => $user->data()->id,
                            "name" => Input::get("name"),
                            "adresse" => Input::get("adresse"),
                            "latitude" => $lat,
                            "longitude" => $lng
                        ));
                        return json_encode(array("success" => true));
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }
                } else {
                    return json_encode(array("success" => false, "error" => "Du hast keine Rechte für diese Aktion"));
                }
            }
        }
        return json_encode(array("success" => false));
    }

    /**
     * get all events created by a specific user id
     * @return string json stirng with success information
     */
    public static function getEvents()
    {
        if (Input::exists("get")) {
            $user = Usercontrol::validate(false, "get");
            // only for organizers
            if ($user && $user->hasRole(User::ROLE_ORGANIZER)) {
                $event = new Event();
                $event->findByUser($user->data()->id);
                return json_encode(array("success" => true, "events" => $event->data()));
            }
        }
        return json_encode(array("success" => false));
    }

    /**
     * deletes an event from id
     * @return string json string with success information
     */
    public static function deleteEvent()
    {
        if (Input::exists()) {
            $id = Input::get("id");
            $token = Input::get("token");
            $res = User::getUserDataFromToken($token);
            $u = new User($res->personenId);
            if ($u->hasRole(User::ROLE_ORGANIZER)) {
                $db = DB::getInstance();

                // also delete all meetings
                $db->get("treffen", array("angebotId", "=", $id));
                $treffenIds = array();
                foreach ($db->results() as $r) {
                    array_push($treffenIds, $r->id);
                }

                // also delete all routes
                foreach ($treffenIds as $t) {
                    $db->get("route_treffen", array("treffenId", "=", $t));
                    if ($db->count())
                        $db->delete("route", array("id", "=", $db->first()->routeId));
                }

                // finally delete event itself
                $db->delete("angebote", array("id", "=", $id));
                if ($db->error()) {
                    return json_encode(array("success" => false, "error" => "Datenbankfehler"));
                }
                return json_encode(array("success" => true));
            } else {
                return json_encode(array("success" => false, "error" => "Unzureichende Rechte"));
            }
        } else {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
        }
    }

    /**
     * get event details such as meetings list and refuses
     * @param string|int $id event id for which the details should be obtained from the DB
     * @return string json string with success information
     */
    public static function getDetails($id)
    {
        if (is_numeric($id)) {
            $event = new Event();
            $event->find($id, true);
            if (!$event->data()) {
                return json_encode(array("success" => false, "error" => "Event existiert nicht!"));
            }
            return json_encode(array("success" => true, "data" => $event->data(), "participants" => $event->participants(), "refuses" => $event->refuses(), "meetings" => $event->meetings()));
        }
        return json_encode(array("success" => false));
    }

    /**
     * function to create meeting by post input data
     * @return string json string with success information
     */
    public static function createMeetings()
    {
        if (Input::exists()) {
            $startdates = json_decode(Input::get("startdates"));
            $enddates = json_decode(Input::get("enddates"));
            $id = Input::get("id");

            $token = Input::get("token");
            $res = User::getUserDataFromToken($token);
            $u = new User($res->personenId);
            if ($u->hasRole(User::ROLE_ORGANIZER)) {
                $db = DB::getInstance();
                // as there might be more than one meeting
                for ($i = 0; $i < count($startdates); $i++) {
                    $start = $startdates[$i];
                    $end = $enddates[$i];
                    $db->insert("treffen", array(
                        "datumStart" => $start,
                        "datumEnde" => $end,
                        "angebotId" => $id
                    ));

                    if ($db->error()) {
                        return json_encode(array("success" => false, "error" => "Datenbankfehler"));
                    }
                }
                return json_encode(array("success" => true));
            } else {
                return json_encode(array("success" => false, "error" => "Unzureichende Rechte"));
            }
        } else {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
        }
    }

    /**
     * deletes a single meeting from the DB
     * @return string json string with success information
     */
    public static function deleteMeeting()
    {
        if (Input::exists()) {
            $id = Input::get("id");
            $token = Input::get("token");
            $res = User::getUserDataFromToken($token);
            $u = new User($res->personenId);
            if ($u->hasRole(User::ROLE_ORGANIZER)) {

                // delete also the route for this meeting
                $db = DB::getInstance();
                $db->get("route_treffen", array("treffenId", "=", $id));
                if ($db->count()) {
                    $db->delete("route", array("id", "=", $db->first()->routeId));
                    $db->delete("route_treffen", array("treffenId", "=", $id));
                }

                // now delete meeting itself
                $db->delete("treffen", array("id", "=", $id));
                if ($db->error()) {
                    return json_encode(array("success" => false, "error" => "Datenbankfehler"));
                }
                return json_encode(array("success" => true));
            } else {
                return json_encode(array("success" => false, "error" => "Unzureichende Rechte"));
            }
        } else {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
        }
    }

    /**
     * add user to list of participating participants (lol)
     * @param string|int $id id of the event the user takes part
     * @return string json string with success information
     */
    public static function registerUser($id)
    {
        if (Input::exists("get")) {
            if (is_numeric($id)) {
                $token = Input::get("token");
                $uid = User::getUserDataFromToken($token);
                $user = new User($uid->personenId);
                if ($user->hasRole(User::ROLE_PARTICIPANT)) {
                    if ($user->isParticipatingOnEvent($id))
                        return json_encode(array("success" => false, "error" => "Du bist bereits für diese Veranstaltung registriert!"));
                    $result = $user->registerEvent($id);

                    // delete route to indicate it has to be recalculated
                    if ($result) {
                        Routecontrol::deleteForEvent($id);
                        return json_encode(array("success" => true));
                    } else {
                        return json_encode(array("success" => false, "error" => "Veranstaltung nicht gefunden!"));
                    }
                } else {
                    return json_encode(array("success" => false, "error" => "Unzureichende Rechte"));
                }
            } else {
                return json_encode(array("success" => false, "error" => "Bitte nur Zahlenwerte für die Identifikationsnummer der Veranstaltung eingeben!"));
            }
        } else {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
        }
    }

    /**
     * removes a user from the participating list
     * @param string|int $eid id of the event from which the user unregisters
     * @return string json string with success information
     */
    public static function unregisterUser($eid)
    {
        if (Input::exists("get")) {
            if (is_numeric($eid)) {
                $token = Input::get("token");
                $uid = User::getUserDataFromToken($token);
                $user = new User($uid->personenId);
                if ($user->hasRole(User::ROLE_PARTICIPANT)) {
                    if (!$user->isParticipatingOnEvent($eid)) {
                        return json_encode(array("success" => true, "error" => "Du nimmst nicht teil!"));
                    } else {
                        $result = $user->unregisterEvent($eid);
                        // delete route to indicate it has to be recalculated
                        Routecontrol::deleteForEvent($eid);
                        if ($result) {
                            return json_encode(array("success" => true));
                        } else {
                            return json_encode(array("success" => false, "error" => "Veranstaltung nicht gefunden!"));
                        }
                    }
                } else {
                    return json_encode(array("success" => false, "error" => "Unzureichende Rechte!"));
                }
            } else {
                return json_encode(array("success" => false, "error" => "Bitte nur Zahlenwerte für die Identifikationsnummer der Veranstaltung eingeben!"));
            }
        } else {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen!"));
        }
    }

    /**
     * adds driver for the event
     * @param string|int $eventId id of the event a driver should drive for
     * @return string json string with success information
     */
    public static function registerDriver($eventId)
    {
        // if statements speak for themselves, no comments needed
        if (!Input::exists("get")) {
            return json_encode(array("success" => false, "error" => "Keine Daten empfagngen!"));
        }
        if (!$token = Input::get("token")) {
            return json_encode(array("success" => false, "error" => "Bitte den Token angeben!"));
        }
        if (!$user = User::getUserDataFromToken($token)) {
            return json_encode(array("success" => false, "error" => "Bitte korrekten Token angeben!"));

        }
        $user = new User($user->personenId);
        if (!$user->hasRole(2)) {
            return json_encode(array("success" => false, "error" => "Nur Fahrer können diese Funktion nutzen!"));
        }

        $db = DB::getInstance();
        $db->get("angebote", array("id", "=", $eventId));
        if (!$db->count()) {
            return json_encode(array("success" => false, "error" => "Es existiert kein Treffen mit dieser ID!"));
        }

        if ($db->first()->standardfahrer != 0) {
            return json_encode(array("success" => false, "error" => "Für dieses Angebot gibt es bereits einen Fahrer!"));
        }

        // set driver for every meeting (current functionalty)
        $db->get("treffen", array("angebotId", "=", $eventId));
        if ($db->count()) {
            $count = $db->count();
            $treffenIds = array();
            foreach ($db->results() as $r) {
                array_push($treffenIds, $r->id);
            }
            for ($i = 0; $i < $count; $i++) {
                $db->insert("fahrtangebot", array("treffenId" => $treffenIds[$i], "personenId" => $user->data()->id));
            }
        }

        // add as standardfahrer
        $db->update("angebote", $eventId, array("standardfahrer" => $user->data()->id));
        Routecontrol::deleteForEvent($eventId);
        return json_encode(array("success" => true));
    }

    /**
     * unregisters a driver for an event by meeting id. Looks for event id assigned to the meeting
     * @param string|int $meetingId id of the meeting where the driver drives for
     * @return string json string with success information
     */
    public static function unregisterDriver($meetingId)
    {
        // no comments needed, if statements speak for themselves
        if (!Input::exists("get")) {
            return json_encode(array("success" => false, "error" => "Keine Daten empfagngen!"));
        }
        if (!$token = Input::get("token")) {
            return json_encode(array("success" => false, "error" => "Bitte den Token angeben!"));
        }
        if (!$user = User::getUserDataFromToken($token)) {
            return json_encode(array("success" => false, "error" => "Bitte korrekten Token angeben!"));

        }
        $user = new User($user->personenId);
        if (!$user->hasRole(2)) {
            return json_encode(array("success" => false, "error" => "Nur Fahrer können diese Funktion nutzen!"));
        }

        $db = DB::getInstance();
        $db->get("treffen", array("id", "=", $meetingId));
        if (!$db->count()) {
            return json_encode(array("success" => false, "error" => "Es existiert kein Treffen mit dieser ID!"));
        }

        // get event id from the meeting
        $eventId = $db->first()->angebotId;
        $db->get("treffen", array("angebotId", "=", $eventId));
        if ($db->count()) {
            $count = $db->count();

            // delete driver for all meetings (current functionality)
            $treffenIds = array();
            foreach ($db->results() as $r) {
                array_push($treffenIds, $r->id);
            }
            for ($i = 0; $i < $count; $i++) {
                $db->delete("fahrtangebot", array("treffenId", "=", $treffenIds[$i], "and", "personenId", "=", $user->data()->id));
                $db->get("route_treffen", array("treffenId", "=", $treffenIds[$i]));
                if ($db->count())
                    $db->delete("route", array("id", "=", $db->first()->routeId));
                $db->delete("route_treffen", array("treffenId" => $treffenIds[$i]));
            }
        }

        // he is no standardfahrer anymore...
        $db->update("angebote", $eventId, array("standardfahrer" => "null"));
        return json_encode(array("success" => true));
    }

    /**
     * checks if the event has a standard driver based on the meeting id
     * @param string|int $meeting meeting id from which it should be checked
     * @return bool true if has false if it hasn't got a standard driver
     */
    public static function meetingHasStandardDriver($meeting)
    {
        $db = DB::getInstance();
        $db->get("treffen", array("id", "=", $meeting));
        if ($db->count()) {
            $event = $db->first()->angebotId;
            $db->get("angebote", array("id", "=", $event));
            if ($db->count()) {
                return $db->first()->standardfahrer;
            }
        }
        return false;
    }

    /**
     * UNUSED
     * @param $id
     * @return bool
     */
    private static function eventHasStandardDriver($id)
    {
        $db = DB::getInstance();
        $db->get("angebote", array("id", "=", $id));
        if ($db->count()) {
            return $db->first()->standardfahrer;
        }
        return false;
    }

}