<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 14.02.2017
 * Time: 18:44
 *
 * Time entity class representing the DB model
 */
class Time
{
    private $_station, $_treffen, $_date, $_id, $_db;

    /**
     * Time constructor.
     * @param string|int $_station routestation id
     * @param string|int $_hour hour
     * @param string|int $_minute minute
     * @param string|int $_id id
     */
    public function __construct($_station = 0, $_treffen = 0, $_date = 0, $_id = 0)
    {
        $this->_db = DB::getInstance();
        $this->_station = $_station;
        $this->_treffen = $_treffen;
        $this->_date = $_date;
        $this->_id = $_id;
    }

    /**
     * insert into DB
     * @param string|int $s routestationId
     * @param string $d mysql datetime string of departure
     */
    public function create($s, $d)
    {
        $this->_db->insert("zeiten", array("routestationId" => $s,"zeit"=> $d));
        $this->_id = $this->_db->lastInsertId();
    }

    /**
     * find a routestation by its id
     * @param string|int $routestation id of the station to be found from
     * @return $this|bool self instance or false if errors occured
     */
    public function findByRsId($routestation) {
        $this->_db->get("zeiten",array("routestationId","=",$routestation));
        if($this->_db->count()){
            $this->_station = $routestation;
            $this->_date = $this->_db->first()->zeit;
            $this->_id = $this->_db->first()->id;
            return $this;
        } else return false;
    }

    /**
     * @return mixed
     */
    public function getTreffen()
    {
        return $this->_treffen;
    }

    /**
     * @param mixed $treffen
     */
    public function setTreffen($treffen)
    {
        $this->_treffen = $treffen;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->_date;
    }

    /**
     * @param int $date
     */
    public function setDate($date)
    {
        $this->_date = $date;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getStation()
    {
        return $this->_station;
    }

    /**
     * @param mixed $station
     */
    public function setStation($station)
    {
        $this->_station = $station;
    }

}