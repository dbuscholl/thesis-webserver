<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 14.02.2017
 * Time: 18:35
 *
 * Route entity class containing the path as stations array and meeting ids for which the route should be applied. Provides
 * methods for time calculation and adding into database
 */
class Route
{
    // for google services
    const API_KEY = "AIzaSyCz4DNxTDf7GCFDPNZS9rL56P6ZUX7TBkI";
    private $_treffenIds = array();
    private $_station = array();
    private $_times = array();
    private $_id;
    private $_db;

    function __construct($routeId = null)
    {
        $this->_db = DB::getInstance();
        if ($routeId) {
            $this->find($routeId);
        }
    }

    /**
     * creates empty route in the database and stores the id into the tables and attributes
     * @param $meetingId
     */
    function create($meetingId)
    {
        $this->_db->insert("route", array("id" => "NULL"));
        $this->_id = $this->_db->lastInsertId();
        $this->_db->insert("route_treffen", array("routeId" => $this->_id, "treffenId" => $meetingId));
        array_push($this->_treffenIds, $meetingId);
    }

    /**
     * inserts path into the database
     * @param array $path array of user ids
     */
    function insertPath($path)
    {
        if (count($path) < 3) {
            return;
        }

        for ($i = 0; $i < count($path); $i++) {
            $p = $path[$i];
            $s = new Station();
            // first or last should store -1 to indicate driver or event location as target
            if ($i == 0 || $i == count($path) - 1)
                $s->create($this->_id, -1, $i);
            else
                $s->create($this->_id, $p, $i);
            array_push($this->_station, $s);
        }
    }

    /**
     * calculates the times using googles distance matrix api
     * @param array $points array of points for which the times should be created
     * @param string $timestring starttime of the event
     */
    function createTimes($points, $timestring)
    {
        // 10 minutes tolerance before starting event
        $timestamp = strtotime($timestring);
        $arrival = strtotime("-10 minutes", $timestamp);

        $string = "";

        // storing request results here, just in case
        $matrices = array();
        $times = array();
        for ($i = 0; $i < count($points); $i++) {
            $point = $points[$i];

            //building parameters
            $string .= $point->getLatitude() . "," . $point->getLongitude() . "|";
            if (($i % 1 == 0 && $i > 0) || $i == count($points) - 1) {
                // remove the last "|" this is not valid for the url
                $string = substr($string, 0, (strlen($string) - 1));
                $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$string&destinations=$string&key=" . Route::API_KEY;
                $json = file_get_contents($url);
                $json = json_decode($json, true);
                if (isset($json["rows"][0]["elements"][1]["duration"]))
                    array_push($times, $json["rows"][0]["elements"][1]["duration"]["value"]);
                array_push($matrices, $json);
                // build for the next point
                $string = $point->getLatitude() . "," . $point->getLongitude() . "|";
            }
        }

        // calculate arrival times from the array of driving times between points
        $time = $arrival;
        $t = new Time();
        $timelist = array();
        $t->create($this->_station[count($this->_station) - 1]->getId(), date("Y-m-d H:i:s", $time));
        array_unshift($timelist, $t);
        // subtract the driving time for each point and store it into the time class
        for ($i = count($times) - 1; $i >= 0; $i--) {
            $amount = $times[$i];
            $t = new Time();
            $time = strtotime("-$amount seconds", $time);
            $t->create($this->_station[$i]->getId(), date("Y-m-d H:i:s", $time));
            array_unshift($timelist, $t);
        }
        $this->_times = $timelist;
    }

    /**
     * incomplete function
     * @param $routeId
     * @return $this|bool
     */
    private function find($routeId)
    {
        $this->_db->get("route", array("id", "=", $routeId));
        if ($this->_db->count()) {
            $this->_id = $this->_db->first()->id;
        } else return false;
        $this->_db->get("route_treffen", array("routeId", "=", $routeId));
        if ($this->_db->count()) {
            foreach ($this->_db->results() as $r) {
                array_push($this->_treffenIds, $r->treffenId);
            }
        } else return false;

        $this->_db->get("routenstationen", array("routenId", "=", $routeId));
        foreach ($this->_db->results() as $r) {
            $s = new Station($r->id, $r->routenId, $r->personenId, $r->reihenfolge);
            array_push($this->_station, $s);

            $t = new Time();
            // TODO: function to get all times for all meetings
            array_push($this->_times, $t);
        }
        return $this;
    }

    /**
     * build route class and all stuff for it from a given meetingid
     * @param string|int $meetingId id of the meeting for which the route should be searched for in the database
     * @return $this|bool self instance or false if any errors occured
     */
    public function byMeetingId($meetingId)
    {
        // get route id
        $this->_db->get("route_treffen", array("treffenId", "=", $meetingId));
        if ($this->_db->count()) {
            array_push($this->_treffenIds, $meetingId);
            $this->_id = $this->_db->first()->routeId;
        } else return false;

        // get routestations
        $this->_db->get("routenstationen", array("routenId", "=", $this->_id));
        if ($this->_db->count()) {
            foreach ($this->_db->results() as $r) {
                $s = new Station($r->id, $r->routenId, $r->personenId, $r->reihenfolge);
                array_push($this->_station, $s);

                // get time for the station
                $t = new Time();
                $t->findByRsId($r->id);
                array_push($this->_times, $t);
            }
        } else return false;

        return $this;

    }

    public function station()
    {
        return $this->_station;
    }

    public function times()
    {
        return $this->_times;
    }

    /*public function byMeetingIdAndDriver($meetingId, $uid)
    {
        $this->_db->get("route_treffen", array("treffenId", "=", $meetingId, "and", "personenId", "=", $uid));
        if ($this->_db->count()) {
            array_push($this->_treffenIds, $meetingId);
            $this->_id = $this->_db->first()->routeId;
        } else return false;

        $this->_db->get("routenstationen", array("routenId", "=", $this->_id));
        if ($this->_db->count()) {
            foreach ($this->_db->results() as $r) {
                $s = new Station($r->id, $r->routenId, $r->personenId, $r->reihenfolge);
                array_push($this->_station, $s);

                $t = new Time();
                $t->findByRsId($r->id);
                array_push($this->_times, $t);
            }
        } else return false;

        return $this;
    }*/

}