<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 14.02.2017
 * Time: 18:35
 *
 * Routecontroller class which executes route function, such as calculating a whole route with all necessary stuff
 */
class Routecontrol
{
    /**
     * calculates a complete route including times for a given meetingid
     * @param string|int $meetingId id of the meeting for which a route should get calculated
     * @return string json string with success information
     */
    public static function calculate($meetingId)
    {
        if (Input::exists()) {
            $meetingId = Input::get("meetingId");
        }
        if (!$user = User::getUserDataFromToken(Input::get("token"))) {
            return json_encode(array("success" => false, "error" => "Du musst angemeldet sein, um diese Aktion durchzuführen"));
        }

        $userid = $user->personenId;
        $db = DB::getInstance();
        $db->get("treffen", array("id", "=", $meetingId));
        if ($db->count()) {

            // get driver data
            $fahrer = new StdClass();
            $eventId = $db->first()->angebotId;
            $meeting = $db->first();
            $db->get("fahrtangebot", array("treffenId", "=", $meetingId));
            if ($db->count()) {
                $u = new User($db->first()->personenId);
                $fahrer = $u->data();
            } else {
                return json_encode(array("success" => false, "error" => "Es gibt keinen Fahrer für diese Veranstaltung!"));
            }

            // get event data
            $e = new Event();
            $e->find($eventId, true);
            $p = $e->participants();
            $r = $e->refuses();
            if (!count($p)) {
                return json_encode(array("success" => false, "error" => "Es gibt keine Teilnehmer für diese Veranstaltung!"));
            }

            // remove participants who refused from the list
            foreach ($r as $refuse) {
                if ($refuse->treffenId != $meetingId) {
                    continue;
                }
                $count = count($p);
                for ($i = 0; $i < $count; $i++) {
                    if ($refuse->personenId == $p[$i]["id"]) {
                        unset($p[$i]);
                    }
                }
            }
            $p = array_values($p); // reassign indices
            $points = array();

            $numParticipants = count($p);
            if ($numParticipants == 0) {
                return json_encode(array("success" => false, "error" => "Teilnehmerliste ist leer"));
            }
            if ($numParticipants == 1) {
                if ($p[0]["id"] == $userid) {
                    return json_encode(array("success" => false, "error" => "Außer dir sind keine Teilnehmer vorhanden!"));
                }
            }

            //create points array for participants list
            for ($i = 0; $i < $numParticipants; $i++) {
                array_push($points, Point::make($p[$i]["id"], $p[$i]["latitude"], $p[$i]["longitude"]));
            }
            // and dont forget to add driver and event location as point
            array_unshift($points, Point::make($fahrer->id, $fahrer->latitude, $fahrer->longitude));
            array_push($points, Point::make($e->data()->id, $e->data()->latitude, $e->data()->longitude));

            // START THE MADNESS!!!
            $dist = new Distance($points);
            $dist->buildMatrix();
            $dist->tspHeuristic(0, count($points) - 1);
            $path = $dist->getPath();
            // path as person ids
            $pathOfPerson = array();
            // path as point instances
            $pathOfPoints = array();
            for ($i = 0; $i < count($path); $i++) {
                array_push($pathOfPerson, $points[$path[$i]]->getId());
                array_push($pathOfPoints, $points[$path[$i]]);
            }

            // create route and calculate times
            $r = new Route();
            $r->create($meetingId);
            $r->insertPath($pathOfPerson);
            $r->createTimes($pathOfPoints, $meeting->datumStart);

            $end = microtime(true);

            return json_encode(array("success" => true));
        }
        return json_encode(array("success" => false, "error" => "Database error!"));

    }

    /**
     * get a complete timetable for a meeting
     * @param string|int $meetingId id of the meeting for which information should be read from the DB
     * @return string json string with success information
     */
    public static function getRoute($meetingId)
    {
        if (!Input::exists("get")) {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen!"));
        }
        if (!$token = Input::get("token")) {
            return json_encode(array("success" => false, "error" => "Bitte korrekten Token angeben!"));
        }

        $uid = User::getUserDataFromToken($token);
        if (!$uid) {
            return json_encode(array("success" => false, "error" => "Du musst angemeldet sein, um diese Aktion nutzen zu können!"));
        }

        $uid = $uid->personenId;
        $r = new Route();
        $route = $r->byMeetingId($meetingId);
        if (!$route) {
            return json_encode(array("success" => false, "error" => "Treffen hat keine Route!"));
        }

        $return = array();
        for ($i = 0; $i < count($route->station()); $i++) {
            $time = $route->times()[$i];
            $station = $route->station()[$i];

            // write time for the station
            $return[$i]["time"] = $time->getDate();
            $pid = $station->getPerson();

            // resolve id of person as valid name
            if ($pid != -1) {
                $u = new User($pid);
                $return[$i]["address"] = $u->data()->adresse;
                $return[$i]["latitude"] = $u->data()->latitude;
                $return[$i]["longitude"] = $u->data()->longitude;
                $return[$i]["name"] = $u->data()->vorname . " " . $u->data()->nachname;

            // if -1 check if first or last point and resolve as driver / event location and add data
            } else {
                if ($station->getOrder() == 0) {
                    $u = new User($uid);
                    $return[$i]["address"] = $u->data()->adresse;
                    $return[$i]["latitude"] = $u->data()->latitude;
                    $return[$i]["longitude"] = $u->data()->longitude;
                    $return[$i]["name"] = $u->data()->vorname . " " . $u->data()->nachname;
                } else {
                    $address = "";
                    $name = "";
                    $lat = 0;
                    $lng = 0;
                    $db = DB::getInstance()->get("treffen", array("id", "=", $meetingId));
                    if (count($db->results())) {
                        $event = new Event($db->first()->angebotId);
                        $name = $event->data()->name;
                        $address = $event->data()->adresse;
                        $lat = $event->data()->latitude;
                        $lng = $event->data()->longitude;
                    }
                    $return[$i]["address"] = $address;
                    $return[$i]["latitude"] = $lat;
                    $return[$i]["longitude"] = $lng;
                    $return[$i]["name"] = $name;
                }
            }
        }

        return json_encode(array("success" => true, "data" => $return));
    }

    /**
     * delete a route for a meeting
     * @param string|int $meetingId the id of the meeting for which route should be deleted
     * @return bool
     */
    public static function delete($meetingId)
    {
        $db = DB::getInstance();
        $db->get("route_treffen", array("treffenId", "=", $meetingId));
        if ($db->count()) {
            $route = $db->first()->routeId;
            $db->delete("route", array("id", "=", $route));
            return true;
        }
        return false;
    }

    /**
     * deletes all routes for all meetings of an event
     * @param string|int $id id of the event for which all routes should be deleted
     * @return bool json string with success information
     */
    public static function deleteForEvent($id)
    {
        $db = DB::getInstance();
        $db->get("treffen", array("angebotId", "=", $id));
        if ($db->count()) {
            $treffenIds = array();
            foreach ($db->results() as $r) {
                array_push($treffenIds, $r->id);
            }
            $routeIds = array();
            foreach ($treffenIds as $t) {
                $db->get("route_treffen", array("treffenId", "=", $t));
                if ($db->count())
                    array_push($routeIds, $db->first()->routeId);
            }
            foreach($routeIds as $r) {
                $db->delete("route",array("id","=",$r));
            }
            return true;
        }
        return false;
    }
}