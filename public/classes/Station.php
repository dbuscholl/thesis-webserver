<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 14.02.2017
 * Time: 18:40
 *
 * Station entity class representating DB model
 */
class Station
{
    private $_id, $_route, $_person, $_order, $_db;

    /**
     * Station constructor.
     * @param int $i self id
     * @param int $r route id
     * @param int $p person id
     * @param int $o order in list
     */
    public function __construct($i = 0, $r = 0, $p = 0, $o = 0)
    {
        $this->_db = DB::getInstance();
        $this->_id = $i;
        $this->_route = $r;
        $this->_person = $p;
        $this->_order = $o;
    }

    public function create($route, $personid, $order)
    {
        $this->_route = $route;
        $this->_person = $personid;
        $this->_order = $order;
        $this->_db->insert("routenstationen", array("routenId" => $route, "personenId" => $personid, "reihenfolge" => $order));
        $this->_id = $this->_db->lastInsertId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }


    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->_route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->_route = $route;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->_person;
    }

    /**
     * @param mixed $person
     */
    public function setPerson($person)
    {
        $this->_person = $person;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->_order = $order;
    }


}