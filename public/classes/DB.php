<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 17.10.2016
 * Time: 22:23
 *
 * Simple but efficient database wrapper to easy execute queries from written in the singleton pattern
 */
class DB
{
    // holding only one instance of the database for more performance based on prepared statments execution
    private static $_instance = null;
    // the pdo object
    private $_pdo,
        // query as string
        $_query,
        // error indicator
        $_error = false,
        // result as stdclass
        $_results,
        // number of results from the database / affected rows
        $_count = 0;

    /**
     * DB constructor. Connects to the database with data from the config class
     */
    private function __construct()
    {
        try {
            $this->_pdo = new PDO("mysql:host=" . Config::get("mysql/host") . ";dbname=" . Config::get("mysql/db") . ";", Config::get("mysql/username"), Config::get("mysql/password"));
            $this->_pdo->exec("set names utf8");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * this is the class from where the instance should be obtained from.
     * @return DB new / existing instance of the DB class
     */
    public static function getInstance()
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    /**
     * executes a query on the database
     * @param string $sql the string to be executed
     * @param array $params the parameters in correct oreder which should be bound on the string
     * @return $this self instance
     */
    public function query($sql, $params = array())
    {
        $this->_error = false;
        //$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        if ($this->_query = $this->_pdo->prepare($sql)) {
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }
            if ($this->_query->execute() !== false) {
                $this->_count = $this->_query->rowCount();
                $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
            } else {
                $this->_error = true;
                var_dump($this->_pdo->errorInfo());
            }
        }

        return $this;
    }

    /**
     * perform an action against the database
     * @param string $action the type (SELECT, INSERT, UPDATE etc.)
     * @param string $table the table which is affected be the action
     * @param array $where containing where clause like ([columnname],[operation],[value],[and/or] and so on...)
     * @param array $order order in which the results should be sorted ([columnname] [ASC/DESC])
     * @param bool $biggernow only for operations with column "datumstart" if only results newer than current date should be displayed
     * @return $this|bool returns self instace or falce if error occured
     */
    public function action($action, $table, $where = array(), $order = array(), $biggernow = false)
    {
        if (count($where) % 4 === 3) {
            $operators = array("=", ">", "<", ">=", "<=", "like", "in", "not in");

            $stack = array();
            $wherestring = "";
            for ($i = 0; $i < count($where); $i++) {
                switch ($i % 4) {
                    //field
                    case 0:
                        $wherestring .= $where[$i];
                        break;
                    //operator
                    case 1:
                        if (!in_array($where[$i], $operators)) {
                            return false;
                        }
                        $wherestring .= " $where[$i]";
                        break;
                    //value
                    case 2:
                        array_push($stack, $where[$i]);
                        $wherestring .= " ?";
                        break;
                    //appendant
                    case 3:
                        if ($where[$i] != "and" && $where[$i] != "or") {
                            return false;
                        }
                        $appendant = $where[$i];
                        $wherestring .= " $appendant ";
                }
            }
            /*
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];
            */

            $orderstring = "";
            if (count($order) === 2) {
                $column = $order[0];
                $sort = $order[1];
                $orderstring = "ORDER BY $column $sort";
            }

//            if (in_array($operator, $operators)) {
            $sql = "{$action} FROM {$table} WHERE {$wherestring} {$orderstring}";
            if ($biggernow) $sql .= " and datumStart > now()";
            if (!$this->query($sql, $stack)->error()) {
                return $this;
            }
//            }
            return $this;
        } else {
            return false;
        }
    }

    /**
     * same as action but with the in operator intead of "where"
     * @param $actionstring $action the type (SELECT, INSERT, UPDATE etc.)
     * @param string $table the table which is affected by the action
     * @param string $field column where the in fucntion of mysql should work
     * @param array $stack list of items which should be searched for
     * @param array $order order in which the results should be sorted ([columnname] [ASC/DESC])
     * @return $this|bool self instance or false if error occured
     */
    public function actionWhereIn($action, $table, $field, $stack, $order = array())
    {
        $orderstring = "";
        if (count($order) === 2) {
            $column = $order[0];
            $sort = $order[1];
            $orderstring = "ORDER BY $column $sort";
        }

        if (count($stack) > 0) {
            // creates a string containing ?,?,?
            $clause = implode(',', array_fill(0, count($stack), "?"));
            $sql = "{$action} FROM {$table} WHERE  {$field} IN (" . $clause . ") {$orderstring}";
            //call_user_func_array(array($stmt, 'bind_param'), $stack);

            if (!$this->query($sql, $stack)->error()) {
                return $this;

            }
            return $this;
        } else {
            return false;
        }
    }


    /**
     * performs a get action against the database
     * @param string $table the table on which the operation should be performed
     * @param array $where array with searching filters containing where clause like ([columnname],[operation],[value],[and/or] and so on...)
     * @param array $order order in which the results should be sorted ([columnname] [ASC/DESC])
     * @param bool $biggernow only for operations with column "datumstart" if only results newer than current date should be displayed
     * @return bool|DB self instance or false if error occured
     */
    public function get($table, $where, $order = array(), $biggernow = false)
    {
        return $this->action("SELECT *", $table, $where, $order, $biggernow);
    }

    /**
     * performs delete query against the database
     * @param string $table the table on which the operation should be performed
     * @param array $where array with searching filters containing where clause like ([columnname],[operation],[value],[and/or] and so on...)
     * @return bool|DBself instance or false if error occured
     */
    public function delete($table, $where)
    {
        return $this->action("DELETE", $table, $where);
    }

    /**
     * performs an insert operation against the database
     * @param string $table the table on which the operation should be performed
     * @param array $fields fields which should be affected ([columnname] => [value])
     * @return bool indicating if it was successful or not
     */
    public function insert($table, $fields = array())
    {
        if (count($fields)) {
            $keys = array_keys($fields);
            $values = '';
            $x = 1;

            foreach ($fields as $field) {
                $values .= '?';
                if ($x < count($fields)) {
                    $values .= ', ';
                }
                $x++;
            }

            $sql = "INSERT INTO `{$table}` (`" . implode('`, `', $keys) . "`) VALUES ({$values})";

            if (!$this->query($sql, $fields)->error()) {
                return true;
            }
        }
        return false;
    }

    /**
     * performs an update operation against the database
     * @param string $table the table on which the operation should be performed
     * @param string|int $id id of the row which should be updated
     * @param array $fields fields which should be affected ([columnname] => [value])
     * @return bool indicating if it was successful or not
     */
    public function update($table, $id, $fields)
    {
        $set = '';
        $x = 1;

        foreach ($fields as $name => $value) {
            $set .= "{$name} = ?";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE `{$table}` SET {$set} WHERE id = {$id}";

        if (!$this->query($sql, $fields)->error()) {
            return true;
        }

        return false;
    }

    public function results()
    {
        return $this->_results;
    }

    public function first()
    {
        return $this->results()[0];
    }

    public function error()
    {
        return $this->_error;
    }

    public function count()
    {
        return $this->_count;
    }

    public function lastInsertId()
    {
        return $this->_pdo->lastInsertId();
    }
}