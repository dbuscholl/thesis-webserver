<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 26.10.2016
 * Time: 13:06
 *
 * this class controls user functions like login process, registration etc.
 */
class Usercontrol
{
    /**
     * registers user for the application
     * @return string json string with success information
     */
    public static function reigster()
    {
        if (Input::exists()) {
            // Input validation
            $validation = new Validation();
            $validation->check($_POST, array(
                'username' => array(
                    'required' => true,
                    'min' => 3,
                    'max' => 20,
                    'unique' => 'personen'
                ),
                'password' => array(
                    'required' => true,
                    'min' => 6
                ),
                'password_again' => array(
                    'required' => true,
                    'matches' => 'password'
                ),
                'email' => array(
                    'required' => true,
                    'max' => 50,
                    'type' => "email",
                    'unique' => "personen"
                ),
                'typecount' => array(
                    'required' => true,
                    'minval' => 1
                )
            ));

            if ($validation->passed()) {
                // actual registration
                $user = new User();
                $salt = Hash::salt(32);
                try {
                    $user->create(array(
                        'username' => Input::get('username'),
                        'passwort' => Hash::make(Input::get('password'), $salt),
                        'salt' => $salt,
                        'email' => Input::get('email'),
                        'joined' => date('Y-m-d H:i:s'),
                    ));

                    // add role depending on checkbox selection from app
                    if (Input::get("organizer")) $user->addRole(1);
                    if (Input::get("driver")) $user->addRole(2);
                    if (Input::get("participant")) $user->addRole(3);
                    return json_encode(array("success" => true));
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            } else {
                $errorstring = "";
                foreach ($validation->errors() as $error) {
                    $errorstring .= $error . "\n";
                }
                return json_encode(array("error" => $errorstring));
            }
        }
    }

    /**
     * login process. Gets username and password and checks it against the ones stored in the DB with the user class.
     * @return string json string with success information
     */
    public static function login()
    {
        if (Input::exists()) {
            $validate = new Validation();
            $validation = $validate->check($_POST, array(
                'username' => array('required' => true),
                'password' => array('required' => true)
            ));
            if ($validation->passed()) {
                $user = new User();
                $login = $user->login(Input::get('username'), Input::get('password'));

                if ($login) {
                    unset($user->data()->passwort);
                    unset($user->data()->salt);
                    $return = array("success" => true, "token" => $login, "roles" => $user->roles(), "data" => $user->data());
                    return json_encode($return);
                } else {
                    return json_encode(array("success" => false, "error" => "Username or password wrong"));
                }
            } else {
                $str = "";
                foreach ($validation->errors() as $error) {
                    $str .= $error . "\n";
                }
                return json_encode(array("success" => false, "error", $str));
            }
        } else {
            return "No Input...";
        }
    }

    /**
     * removes the token from the personen_session table so future validations with token will not pass and the clients
     * redirect to their login pages
     * @return string json string with success information
     */
    public static function logout()
    {
        if (Input::exists()) {
            $token = Input::get("token");
            $user = User::getUserDataFromToken($token);
            if ($user) {
                $id = $user->personenId;
                DB::getInstance()->delete("personen_session", array("personenId", "=", $id));
                return json_encode(array("success" => true));
            }
        }
        return json_encode(array("success" => false));
    }

    /**
     * valdates a token. Check it against the one stored in the DB.
     * @param bool $directReturn dont json encode if flag is set
     * @param string $type token stored as post or get parameter?
     * @param bool $renew renew token after check if flag is set
     * @return array|bool|string|User if renew => user class instance if directreturn, jsonstring if not. if not renew => false if directreturn and user class instance if found, jsonstring if no directreturn
     */
    public static function validate($directReturn, $type = "post", $renew = false)
    {
        if (Input::exists($type)) {
            $token = Input::get("token");
            if (!$token) {
                die("no token given");
            }
            $user = User::getUserDataFromToken($token);
            if ($user) {
                $time = $user->expire;
                if (time() > $time) {
                    if ($directReturn)
                        return json_encode(array("success" => false, "login" => false));
                    else
                        return false;
                } else {
                    $u = new User($user->personenId);
                    if ($renew) {
                        $token = $u->renewSession($user->id);
                        if ($directReturn) {
                            unset($u->data()->passwort);
                            unset($u->data()->salt);
                            return json_encode(array("success" => true, "token" => $token, "roles" => $u->roles(), "data" => $u->data()));
                        } else
                            return $u;
                    } else {
                        if ($directReturn)
                            return json_encode(array("success" => true, "roles" => $u->roles()));
                        else
                            return $u;
                    }
                }
            }
        }
        if ($directReturn)
            return json_encode(array("success" => false, "login" => false));
        else
            return array("success" => false, "login" => false);
    }

    /**
     * update some user data by post data fields
     * @return string json string with success information
     */
    public static function update()
    {
        if (!Input::exists()) {
            return json_encode(array("success" => false, "error" => "Keine Daten empfangen!"));
        }

        $uid = User::getUserDataFromToken(Input::get("token"));
        if(!$uid){
            return json_encode(array("success" => false, "error" => "Falscher Token!!"));
        }
        $user = new User($uid->personenId);
        unset($_POST["token"]);

        $params = array();
        foreach ($_POST as $key => $value) {
            // some special things like geocoding need to be done if key is "address"
            if ($key == "adresse") {
                $urlparam = str_replace(' ', '+', $value);
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $urlparam;
                $json = file_get_contents($url);
                $json = json_decode($json, true);
                $params["latitude"] = $json["results"][0]["geometry"]["location"]["lat"];
                $params["longitude"] = $json["results"][0]["geometry"]["location"]["lng"];
            }
            $params[$key] = $value;
        }

        if (!$user->update($params, $user->data()->id)) {
            return json_encode(array("success" => false, "error" => "Beim Update trat ein Fehler auf!"));
        }

        return json_encode(array("success" => true));
    }

    // unused
    private static function changePassword()
    {
        $user = new User();
        if (Input::exists()) {
            $validate = new Validation();
            $validate->check($_POST, array(
                'password_current' => array(
                    'required' => true,
                    'min' => 6
                ),
                'password_new' => array(
                    'required' => true,
                    'min' => 6
                ),
                'password_new_again' => array(
                    'required' => true,
                    'min' => 6,
                    'match' => 'password_new'
                )
            ));
            if ($validate->passed()) {
                if (Hash::make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password) {
                    echo "Your current password is wrong";
                } else {
                    $salt = Hash::salt(32);
                    try {
                        $user->update(array(
                            'password' => Hash::make(Input::get('password_new'), $salt),
                            'salt' => $salt
                        ));
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }
                }
            } else {
                foreach ($validate->errors() as $error) {
                    echo $error, '<br';
                }
            }
        }
    }

    /**
     * gets all events the user is participating to
     * @return string json string with success information
     */
    public static function getParticipatingEvents()
    {
        if (Input::exists("get")) {
            if ($token = Input::get("token")) {
                $user = User::getUserDataFromToken($token);
                $user = new User($user->personenId);
                if ($user->hasRole(3)) {
                    $events = $user->getParticipatingEvents();
                    return json_encode(array("success" => true, "events" => $events));
                } else {
                    return json_encode(array("success" => false, "error" => "Keine Rechte für Zugriff"));
                }
            }
        }
        return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
    }

    /**
     * refuses a meeting
     * @param string|int $meeting id of the meeting which the user wants to refuse
     * @return string json string with success information
     */
    public static function refuseMeeting($meeting)
    {
        if (Input::exists("get")) {
            if ($token = Input::get("token")) {
                $user = User::getUserDataFromToken($token);
                $user = new User($user->personenId);
                if ($user->hasRole(3)) {
                    $success = $user->refuseMeeting($meeting);
                    Routecontrol::delete($meeting);
                    if ($success !== false)
                        return json_encode(array("success" => true, "refuses" => $success));
                    else
                        return json_encode(array("success" => false, "error" => "Du hast bereits abgesagt!"));
                } else {
                    return json_encode(array("success" => false, "error" => "Keine Rechte für Zugriff"));
                }
            }
        }
        return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
    }

    /**
     * accepts a meeting
     * @param string|int $meeting id of the meeting which the user wants to accept
     * @return string json string with success information
     */
    public static function acceptMeeting($meeting)
    {
        if (Input::exists("get")) {
            if ($token = Input::get("token")) {
                $user = User::getUserDataFromToken($token);
                $user = new User($user->personenId);
                if ($user->hasRole(3)) {
                    $success = $user->acceptMeeting($meeting);
                    Routecontrol::delete($meeting);
                    if ($success !== false)
                        return json_encode(array("success" => true, "refuses" => $success));
                    else
                        return json_encode(array("success" => false, "error" => "Du hast bereits zugesagt!"));
                } else {
                    return json_encode(array("success" => false, "error" => "Keine Rechte für Zugriff"));
                }
            }
        }
        return json_encode(array("success" => false, "error" => "Keine Daten empfangen"));
    }

    /**
     * get all meetings the user is driving for
     * @return string json string with success information
     */
    public static function getDrivingEvents()
    {
        if (!Input::exists("get")) {
            return json_encode(array("success" => false, "error" => "Keine Daten empfagngen!"));
        }
        if (!$token = Input::get("token")) {
            return json_encode(array("success" => false, "error" => "Bitte den Token angeben!"));
        }
        if (!$user = User::getUserDataFromToken($token)) {
            return json_encode(array("success" => false, "error" => "Bitte korrekten Token angeben!"));

        }
        $user = new User($user->personenId);
        if (!$user->hasRole(2)) {
            return json_encode(array("success" => false, "error" => "Nur Fahrer können diese Funktion nutzen!"));
        }


        $meetingsResult = $user->getDrivingEvents();
        if (!$meetingsResult) {
            return json_encode(array("success" => true, "error" => "Du bist für keine Events registriert"));
        }
        $meetingslist = array();
        foreach ($meetingsResult as $m) {
            // if there are no more infos about the meeting
            $mdata = Event::getMeetingData($m->treffenId);
            if (!$mdata) {
                continue;
            }

            // show only future meetings
            if(strtotime($mdata->datumStart) < time()) {
                continue;
            }
            $eobj = new Event($mdata->angebotId);
            $meeting = $mdata;
            $meeting->angebotName = $eobj->data()->name;

            // get route for the meeting
            $r = new Route();
            $r->byMeetingId($m->treffenId);
            $meeting->stationen = count($r->station());

            // ... including the driving time in full minutes
            if(count($r->times())) {
                $to_time = strtotime($r->times()[count($r->times()) - 1]->getDate());
                $from_time = strtotime($r->times()[0]->getDate());
                $meeting->durationMinutes = round(abs($to_time - $from_time) / 60, 0) . "min";
            }
            else {
                $meeting->durationMinutes = "0min";
            }

            array_push($meetingslist, $meeting);
        }

        usort($meetingslist, function ($a, $b) {
            return (strtotime($a->datumStart) - strtotime($b->datumStart));
        });
        return json_encode(array("success" => true, "data" => $meetingslist));
    }


}