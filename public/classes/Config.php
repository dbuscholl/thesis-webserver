<?php

/**
 * Class Config
 * Simple Class to get configuration items like token expiry in an easy way (e.g. "token/expiry")
 */
class Config
{

    /**
     * @param string $path the path inside the config
     * @return bool|string the value or false if nothing found
     */
    public static function get($path = null) {
        if($path){
            $config = $GLOBALS["config"];
            $path = explode("/", $path);

            foreach($path as $bit) {
                if(isset($config[$bit])) {
                    $config = $config[$bit];
                }
            }

            return $config;
        }
        return false;
    }
}