<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 26.10.2016
 * Time: 12:41
 *
 * User entity class representing the DB Data Model
 */
class User
{
    const ROLE_ORGANIZER = 1;
    const ROLE_DRIVER = 2;
    const ROLE_PARTICIPANT = 3;
    private $_db, $_data, $_isLoggedIn, $_roles;

    public function __construct($user = null)
    {
        $this->_db = DB::getInstance();
        if ($user) {
            $this->find($user);
        }
    }

    /**
     * gets user data from the database by the given id
     * @param string|int $user id of the user of which the data should be found
     * @return bool true if found false if not
     */
    public function find($user = null)
    {
        if ($user) {
            $field = (is_numeric($user)) ? 'id' : 'username';
            $data = $this->_db->get('personen', array($field, '=', $user));
            if ($data->count()) {
                $this->_data = $data->first();
                $roles = $this->_db->get("personen_rollen", array("personenId", "=", $this->data()->id));
                if ($roles->count()) {
                    $this->_roles = $roles->results();
                }
                return true;
            }
        }

        return false;
    }

    /**
     * returns the row of the personen_session table where the token matches the given one
     * @param string $token token to find the match in the DB
     * @return bool StdClass of the first row or falls if errors occured
     */
    public static function getUserDataFromToken($token = null)
    {
        if ($token) {
            $db = DB::getInstance();
            $db->get("personen_session", array("hash", "=", $token));
            if ($db->count() > 0) {
                return $db->first();
            }
        }
        return false;
    }

    /**
     * creates a new user in the database with the given fields
     * @param array $fields fields matching the columns in the database
     * @return $this self instance
     * @throws Exception if any errors occured
     */
    public function create($fields)
    {
        if (!$this->_db->insert("personen", $fields)) {
            throw new Exception('There was a problem creating an account');
        }
        $this->find($fields["username"]);
        return $this;
    }

    /**
     * updates specific columns of a specific user
     * @param array $fields columns to overwrite values with
     * @param string|int $id id of the user of which the data should be updated
     * @return bool true if succeded, false if not
     */
    public function update($fields = array(), $id = null)
    {
        if (!$id) {
            $id = $this->data()->id;
        }

        if (!$this->_db->update('personen', $id, $fields)) {
            return false;
        }
        return true;
    }

    /**
     * login process. Comparing given password with the one stored in the database and generate token which will be stored in
     * database and send back to caller.
     * @param string $username
     * @param string $password
     * @return bool|string token as string if success false if not
     */
    public function login($username = null, $password = null)
    {
        if (!$username && !$password && $this->exists()) {
            return false;
        } else {
            $user = $this->find($username);
            if ($user) {
                if ($this->data()->passwort === Hash::make($password, $this->data()->salt)) {
                    $roles = $this->_db->get("personen_rollen", array("personenId", "=", $this->data()->id));
                    if ($roles->count()) {
                        $this->_roles = $roles->results();
                    }
                    $hashCheck = $this->_db->get('personen_session', array('personenId', '=', $this->data()->id));
                    if (!$hashCheck->count()) {
                        $hash = Hash::secureToken();
                        $this->_db->insert('personen_session', array(
                            'personenId' => $this->data()->id,
                            'hash' => $hash,
                            'expire' => time() + Config::get("remember/cookie_expiry")
                        ));
                    } else {
                        $hash = $hashCheck->first()->hash;
                        $this->_db->update("personen_session", $hashCheck->first()->id, array('expire' => time() + Config::get("remember/cookie_expiry")));
                    }

                    return $hash;
                }
            }
        }
        return false;
    }

    public function exists()
    {
        return (!empty($this->data())) ? true : false;
    }

    public function data()
    {
        return $this->_data;
    }

    public function isLoggedin()
    {
        return $this->_isLoggedIn;
    }

    /**
     * creates new token and stores it in the db
     * @param string|int $id id of the user of which the token should get renewed
     * @return bool|string generated token or false if errors occured
     */
    public function renewSession($id = null)
    {
        if ($id) {
            $token = Hash::secureToken();
            $this->_db->update("personen_session", $id, array("hash" => $token, "expire" => time() + Config::get("remember/cookie_expiry")));
            return $token;
        }
        return false;
    }

    public function roles()
    {
        return $this->_roles;
    }

    /**
     * checks if the user has a specific role
     * @param int $role the role if should check for. use the constants!
     * @return bool true if he has, false if not
     */
    public function hasRole($role)
    {
        for ($i = 0; $i < count($this->roles()); $i++) {
            if ($this->roles()[$i]->rolleId == $role) {
                return true;
            }
        }
        return false;
    }

    /**
     * adds a role to the database for the user
     * @param int $int the role it should add. Use the constants!
     * @throws Exception if any errors occured
     */
    public function addRole($int)
    {
        if (!$this->_db->insert("personen_rollen", array("personenId" => $this->data()->id, "rolleId" => $int))) {
            throw new Exception("There was a problem adding roles!");
        }
    }

    /**
     * creates array of events for which the user participates.
     * @return array Event objects containing undetailed event info
     */
    public function getParticipatingEvents()
    {
        $this->_db->get("teilnehmer", array("personenId", "=", $this->data()->id));
        if (!$this->_db->error()) {
            $events = array();
            $results = $this->_db->results();
            foreach ($results as $result) {
                $e = new Event($result->angebotId);
                array_push($events, $e->data());
            }
            return $events;
        } else {
            return array();
        }
    }

    /**
     * make the user refuse a meeting
     * @param string|int $meeting meeting he refuses
     * @return bool|int number of refuses or false if errors occured
     */
    public function refuseMeeting($meeting)
    {
        $id = $this->data()->id;
        $db = $this->_db->get("nichtAbholen", array("treffenId", "=", $meeting, "and", "personenId", "=", $id));
        if ($db->count()) {
            return false;
        } else {
            $db->insert("nichtAbholen", array("personenId" => $id, "treffenId" => $meeting));
            $db->get("nichtAbholen", array("treffenId", "=", $meeting));
            return $db->count();
        }
    }

    /**
     * make the user accept a meeting
     * @param string|int $meeting id of the meeting he accepts
     * @return bool|int number of accepts or false if errors occured
     */
    public function acceptMeeting($meeting)
    {
        $id = $this->data()->id;
        $db = $this->_db->get("nichtAbholen", array("treffenId", "=", $meeting, "and", "personenId", "=", $id));
        if (!$db->count()) {
            return false;
        } else {
            $db->delete("nichtAbholen", array("treffenId", "=", $meeting, "and", "personenId", "=", $id));
            $db->get("nichtAbholen", array("treffenId", "=", $meeting));
            return $db->count();
        }
    }

    /**
     * registers the user for an event
     * @param string|int $event id of the event to add user to participating list
     * @return bool success indicator
     */
    public function registerEvent($event)
    {
        $id = $this->data()->id;
        $this->_db->insert("teilnehmer", array("angebotId" => $event, "personenId" => $id));
        if (!$this->_db->error()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * checks wether user participates on an event or not
     * @param string|int $id id of the event to check against
     * @return bool true if he is participating, false if not
     */
    public function isParticipatingOnEvent($id)
    {
        $uid = $this->data()->id;
        $this->_db->get("teilnehmer", array("personenId", "=", $uid, "and", "angebotId", "=", $id));
        if (!$this->_db->error()) {
            if ($this->_db->count())
                return true;
            else return false;
        } else return false;
    }

    /**
     * unregisters user for an event
     * @param string|int $eid id of the event to remove user from participating list
     * @return bool success indicator
     */
    public function unregisterEvent($eid)
    {
        $id = $this->data()->id;
        $this->_db->delete("teilnehmer", array("angebotId", "=", $eid, "and", "personenId", "=", $id));
        if (!$this->_db->error()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get rows of the meetings the user drives for. Should only be used by drivers
     * @return array(StdClass)|bool results or false if errors occured
     */
    public function getDrivingEvents()
    {
        $this->_db->get("fahrtangebot", array("personenId", "=", $this->data()->id));
        if ($this->_db->count()) {
            return $this->_db->results();
        } else return false;
    }
}