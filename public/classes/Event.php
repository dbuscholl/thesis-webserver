<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 21.11.2016
 * Time: 14:18
 *
 * Event entity class where database operations can take place.
 */
class Event
{
    private $_db, $_data, $_participants, $_meetings, $_refuses;

    /**
     * Event constructor. Setting database instance
     * @param null $event if set the id of the event will be searched in the database and result information will be inserted into parameters (no details)
     */
    public function __construct($event = null)
    {
        $this->_db = DB::getInstance();
        if ($event) {
            $this->find($event);
        }
    }

    /**
     * looks for the event with the given id in the database and fills parameters with the information if found
     * @param string|int $event id of the event
     * @param bool $detail indicator wheter detailed information such as meetings list or refuses should be obtained aswell
     * @return $this|bool self instace or false if errors occured
     */
    public function find($event, $detail = false)
    {
        if (!is_numeric($event)) {
            return false;
        }
        $this->_db->get("angebote", array("id", "=", $event));
        if ($this->_db->count()) {
            $this->_data = $this->_db->first();

            if ($detail) {
                $this->_db->get("treffen", array("angebotId", "=", $event), array(), true);
                $meetingsIds = array(-1);
                $this->_meetings = $this->_db->results();
                $i = 0;
                // adding also driver names into meetingslist
                foreach ($this->_meetings as $meeting) {
                    $this->_db->get("fahrtangebot", array("treffenId", "=", $meeting->id));
                    if ($this->_db->count()) {
                        $u = new User($this->_db->first()->personenId);
                        $this->_meetings[$i]->driverfullname = $u->data()->vorname . " " . $u->data()->nachname;
                        $this->_meetings[$i]->driverusername = $u->data()->username;
                    } else {
                        $this->_meetings[$i]->driverfullname = "";
                        $this->_meetings[$i]->driverusername = "Noch kein Fahrer eingetragen!";
                    }
                    $i++;
                    array_push($meetingsIds, $meeting->id);
                }
                $this->_db->actionWhereIn("SELECT *", "nichtAbholen", "treffenId", $meetingsIds);
                $this->_refuses = $this->_db->results();

                $this->_db->get("teilnehmer", array("angebotId", "=", $event));
                $this->_participants = array();
                $ids = $this->_db->results();

                foreach ($ids as $result) {
                    $u = new User($result->personenId);
                    array_push($this->_participants, array("id" => $u->data()->id, "username" => $u->data()->username, "vorname" => $u->data()->vorname, "nachname" => $u->data()->nachname, "adresse" => $u->data()->adresse, "latitude" => $u->data()->latitude, "longitude" => $u->data()->longitude));
                }
            }
            return $this;
        }
        return false;
    }

    public static function getMeetingData($meetingId)
    {
        $db = DB::getInstance();
        $db->get("treffen", array("id", "=", $meetingId));
        if ($db->count()) {
            return $db->first();
        } else return false;
    }

    /**
     * creates an event for the database
     * @param array $fields fields to be inserted into the database
     * @return bool indicator wether it was successfull
     * @throws Exception if any errors occured
     */
    public function create($fields)
    {
        if (!$this->_db->insert("angebote", $fields)) {
            throw new Exception("Failed to insert into database");
        }
        return true;
    }

    /**
     * finds all events for the user with the given id ordered by name asc
     * @param string|int $user id of the user of whom the events should be obtained
     * @return $this|bool self instance or false if any errors occured
     */
    public function findByUser($user)
    {
        if (!is_numeric($user)) {
            return false;
        }
        $this->_db->get("angebote", array("personenId", "=", $user), array("name", "asc"));
        $this->_data = $this->_db->results();
        return $this;
    }

    public function data()
    {
        return $this->_data;
    }

    public function count()
    {
        return count($this->_data);
    }

    public function participants()
    {
        return $this->_participants;
    }

    public function refuses()
    {
        return $this->_refuses;
    }

    public function meetings()
    {
        return $this->_meetings;
    }
}