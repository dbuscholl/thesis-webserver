<?php

/**
 * Created by PhpStorm.
 * User: David Buscholl
 * Date: 16.02.2017
 * Time: 11:15
 *
 * This class is the major thing in calculating the shortest way through a list of points where all should be walked through
 * TSP Travelling Salesman Problem
 */
class Distance
{
    //distance matrix where all distances from every point to every other point is stored ([1][3] distance from point 1 to point 3)
    private $_matrix = array();
    //array conatining indices of the points in the shortest way through all points
    private $_path = array();
    // all points which should be walked throguh
    private $_points = array();
    //path in distances instead of point indices for calculation of total length of the path
    private $_distances = array();

    /**
     * Distance constructor.
     * @param array $points all points for which the distances should get calculated
     */
    public function __construct($points)
    {
        $this->_points = $points;
        for ($i = 0; $i < count($points); $i++) {
            $this->_matrix[$i] = array();
            $this->_matrix[$i][$i] = 0;
        }
    }

    /**
     * starting the tsp heuristics calculation.
     * @param int $start starting point index
     * @param int $end ending point index
     */
    public function tspHeuristic($start, $end)
    {
        // simple validation
        $numberPoints = count($this->_points);
        if ($start < 0 || $start >= $numberPoints || $end < 0 || $end >= $numberPoints) {
            return;
        }

        // set start and end as initial values
        $visited = array($start, $end);
        // start with the end because end -> last point should have lower distance than start -> first point
        $currentPoint = $end;

        // for every point in the array
        for ($j = 1; $j < $numberPoints; $j++) {
            for ($i = 0; $i < count($this->_matrix[$currentPoint]); $i++) {
                // get array containing distances to the other points for current point
                $a = $this->_matrix[$currentPoint];
                // sort by distance ascending for easier access
                sort($a);
                // starting with the lowest distance
                foreach ($a as $item) {
                    //get its point index from distance value in the matrix
                    $index = array_search($item, $this->_matrix[$currentPoint]);
                    // check if this distance is already visitied and search another one if so
                    if (array_search($index, $visited) !== false) {
                        continue;
                    } else {
                        break;
                    }
                }

                // double check, because the loop might just have ended
                if (array_search($index, $visited) === false) {
                    // add to arrays, mark as visited and set the found point as current point
                    array_push($this->_distances, $this->_matrix[$currentPoint][$index]);
                    array_push($visited, $index);
                    array_push($this->_path, $index);
                    $currentPoint = $index;
                }
            }
        }

        // now add startpoint as last one
        array_push($this->_path, $start);
        // reverse array, so start will be the first one now!
        $this->_path = array_reverse($this->_path);
        // add endpoint as last point
        array_push($this->_path, $end);

    }

    /**
     * builds the distance matrix from the given points. Calculates each distance to every point
     */
    public function buildMatrix()
    {
        for ($i = 0; $i < count($this->_points); $i++) {
            $p1la = $this->_points[$i]->getLatitude();
            $p1lo = $this->_points[$i]->getLongitude();
            for ($j = $i + 1; $j < count($this->_points); $j++) {
                $p2la = $this->_points[$j]->getLatitude();
                $p2lo = $this->_points[$j]->getLongitude();
                $distance = $this->distance($p1la, $p1lo, $p2la, $p2lo, "K");
                $this->_matrix[$i][$j] = $distance;
                $this->_matrix[$j][$i] = $distance;
            }
        }
    }

    /**
     * Simple distance calculation function. Calculating as straight line
     * @param float $lat1 latitude of point 1
     * @param float $lon1 longitude of point 1
     * @param float $lat2 latitude of point 2
     * @param float $lon2 longitude of point 2
     * @param string $unit distance unit "K" for Kilometer, "N" for nautic miles and everything else or empty for miles
     * @return float distance as float in given unit
     */
    public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        // Kilometers
        if ($unit == "K") {
            return ($miles * 1.609344);

            // Nautic Miles
        } else if ($unit == "N") {
            return ($miles * 0.8684);

            // Miles
        } else {
            return $miles;
        }
    }

    /**
     * @return array
     */
    public function getMatrix()
    {
        return $this->_matrix;
    }

    /**
     * @return array
     */
    public function getPath()
    {
        return $this->_path;
    }


}