<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

ini_set('display_errors', 'On');
error_reporting(E_ALL);

require '../vendor/autoload.php';
require_once 'init.php';

$app = new \Slim\App;
$app->get('/user/{id}/meetings', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $response->getBody()->write("User $id requestet meetings list");

    return $response;
});

$app->post('/user/login', function (Request $request, Response $response) {
    $return = Usercontrol::login();
    $response->getBody()->write($return);
});

$app->post("/user/logout", function (Request $request, Response $response) {
    $return = Usercontrol::logout();
    $response->getBody()->write($return);
});

$app->post("/user/update", function (Request $request, Response $response) {
    $return = Usercontrol::update();
    $response->getBody()->write($return);
});

$app->post("/user", function (Request $request, Response $response) {
    $return = Usercontrol::reigster();
    $response->getBody()->write($return);
});

$app->post("/user/validateLogin", function (Request $request, Response $response) {
    $return = Usercontrol::validate(true, "post", true);
    $response->getBody()->write($return);
});

$app->get("/user/driving", function (Request $request, Response $response) {
    $return = Usercontrol::getDrivingEvents();
    $response->getBody()->write($return);
});

$app->get("/user/driving/{id}", function (Request $request, Response $response) {
    $return = Routecontrol::getRoute($request->getAttribute("id"));
    $response->getBody()->write($return);
});

$app->get("/participating", function (Request $request, Response $response) {
    $response->getBody()->write(Usercontrol::getParticipatingEvents());
});

$app->get("/participating/refuse/{id}", function (Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $return = Usercontrol::refuseMeeting($id);
    $response->getBody()->write($return);
});

$app->get("/participating/accept/{id}", function (Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $return = Usercontrol::acceptMeeting($id);
    $response->getBody()->write($return);
});

$app->post("/events", function (Request $request, Response $respone) {
    $return = Eventcontrol::newEvent();
    $respone->getBody()->write($return);
});

$app->get("/events", function (Request $request, Response $response) {
    $return = Eventcontrol::getEvents();
    $response->getBody()->write($return);
});

$app->post("/events/delete", function (Request $request, Response $response) {
    $return = Eventcontrol::deleteEvent();
    $response->getBody()->write($return);
});

$app->get("/events/{id}/details", function (Request $request, Response $respone) {
    $id = $request->getAttribute('id');
    $return = Eventcontrol::getDetails($id);
    $respone->getBody()->write($return);
});

$app->get("/events/{id}/register", function (Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $return = Eventcontrol::registerUser($id);
    $returnarr = json_decode($return);
    if ($returnarr->success === false)
        $response->getBody()->write($return);
    else {
        $e = Usercontrol::getParticipatingEvents();
        $response->getBody()->write($e);
    }
});

$app->get("/events/{id}/unregister", function (Request $request, Response $response) {
    $eid = $request->getAttribute("id");
    $return = Eventcontrol::unregisterUser($eid);
    $response->getBody()->write($return);
});

$app->get("/events/{id}/registerDriver", function (Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $return = Eventcontrol::registerDriver($id);
    $response->getBody()->write($return);
});

$app->get("/events/{id}/unregisterDriver", function (Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $return = Eventcontrol::unregisterDriver($id);
    $response->getBody()->write($return);
});

$app->post("/meetings", function (Request $request, Response $response) {
    $return = Eventcontrol::createMeetings();
    $response->getBody()->write($return);
});

$app->post("/meetings/delete", function (Request $request, Response $response) {
    $return = Eventcontrol::deleteMeeting();
    $response->getBody()->write($return);
});

$app->get("/meetings/{id}/route", function (Request $request, Response $response) {
    $return = Routecontrol::getRoute($request->getAttribute("id"));
    $response->getBody()->write($response);
});

$app->get("/driver/route/{id}", function (Request $request, Response $response) {
    $id = $request->getAttribute("id");
    $return = Routecontrol::calculate($id);
    $response->getBody()->write($return);
});


$app->get("/request", function (Request $request, Response $response) {
    phpinfo();
});

$app->run();