<?php
session_start();
$GLOBALS["config"] = array(
    'mysql' => array(
        'host' => '127.0.0.1',
        'username' => 'thesis',
        'password' => 'tC26k2x8',
        'db' => 'thesis'
    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 2592000
    )
);

spl_autoload_register(function($class){
    require_once "classes/" . $class . ".php";
});
require_once 'functions/sanitize.php';
